public class Player {
    private String name;
    private int finalScore;

    // -------------------------------------------------------------------------
    
    public Player(String name, int finalScore) {
        this.name = name;
        this.finalScore = finalScore;
    } // end constructor
    
    // -------------------------------------------------------------------------
    
    public String getName() {
        return name;
    } // end method
    
    // -------------------------------------------------------------------------

    public void setName(String name) {
        this.name = name;
    } // end method
    
    // -------------------------------------------------------------------------

    public int getFinalScore() {
        return finalScore;
    } // end method
    
    // -------------------------------------------------------------------------

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    } // end method
} // end class