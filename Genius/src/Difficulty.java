public class Difficulty {
    private int level;
    private int speed;
    private int sequenceSize;

    // -------------------------------------------------------------------------

    public Difficulty(int level, int speed, int sequenceSize) {
        this.level = level;
        this.speed = speed;
        this.sequenceSize = sequenceSize;
    } // end constructor
    
    // -------------------------------------------------------------------------
    
    public int getLevel() {
        return level;
    } // end method

    // -------------------------------------------------------------------------
    
    public void setLevel(int level) {
        this.level = level;
    } // end method

    // -------------------------------------------------------------------------
    
    public int getSpeed() {
        return speed;
    } // end method

    // -------------------------------------------------------------------------

    public void setSpeed(int speed) {
        this.speed = speed;
    } // end method

    // -------------------------------------------------------------------------
    
    public int getSequenceSize() {
        return sequenceSize;
    } // end method

    // -------------------------------------------------------------------------
    
    public void setSequenceSize(int sequenceSize) {
        this.sequenceSize = sequenceSize;
    } // end method
} // end class