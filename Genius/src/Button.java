import java.awt.Color;
import java.awt.Point;
import org.jfugue.player.Player;

public class Button {
    private String label;
    private Color color;
    private Point position;
    private Boolean state;
    private int function;
    private Player sound;

    //--------------------------------------------------------------------------
    
    public Button(String label, Color color, Point position, Boolean state, int function, Player sound) {
        this.label = label;
        this.color = color;
        this.position = position;
        this.state = state;
        this.function = function;
        this.sound = sound;
    } // end constructor
    
    //--------------------------------------------------------------------------

    public String getLabel() {
        return label;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setLabel(String label) {
        this.label = label;
    } // end method
    
    //--------------------------------------------------------------------------

    public Color getColor() {
        return color;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setColor(Color color) {
        this.color = color;
    } // end method
    
    //--------------------------------------------------------------------------

    public Point getPosition() {
        return position;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setPosition(Point position) {
        this.position = position;
    } // end method
    
    //--------------------------------------------------------------------------

    public Boolean getState() {
        return state;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setState(Boolean state) {
        this.state = state;
    } // end method
    
    //--------------------------------------------------------------------------

    public int getFunction() {
        return function;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setFunction(int function) {
        this.function = function;
    } // end method
    
    //--------------------------------------------------------------------------

    public Player getSound() {
        return sound;
    } // end method
    
    //--------------------------------------------------------------------------

    public void setSound(Player sound) {
        this.sound = sound;
    } // end method
} // end class