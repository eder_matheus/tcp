import java.awt.Color;
import java.awt.Point;

public class Scoreboard {
    private int partialScore;
    private Color color;
    private Point position;

    //--------------------------------------------------------------------------

    public Scoreboard(int partialScore, Color color, Point position) {
        this.partialScore = partialScore;
        this.color = color;
        this.position = position;
    } // end constructor
    
    //--------------------------------------------------------------------------
    
    public int getPartialScore() {
        return partialScore;
    } // end method

    //--------------------------------------------------------------------------
    
    public void setPartialScore(int partialScore) {
        this.partialScore = partialScore;
    } // end method

    //--------------------------------------------------------------------------
    
    public Color getColor() {
        return color;
    } // end method

    //--------------------------------------------------------------------------
    
    public void setColor(Color color) {
        this.color = color;
    } // end method

    //--------------------------------------------------------------------------
    
    public Point getPosition() {
        return position;
    } // end method

    //--------------------------------------------------------------------------
    
    public void setPosition(Point position) {
        this.position = position;
    } // end method
    
    //--------------------------------------------------------------------------
    
    public void increaseScore(int increase) {
        this.partialScore += increase;
    } // end method
    
    //--------------------------------------------------------------------------
    
    public void decreaseScore(int decrease) {
        this.partialScore -= decrease;
    } // end method  
} // end class