public class Configurations {
    private Difficulty difficulty;
    private int volume;
    private int attempts;
    private Boolean keyboardMode;

    // -------------------------------------------------------------------------

    public Configurations(Difficulty difficulty, int volume, int attempts, Boolean keyboardMode) {
        this.difficulty = difficulty;
        this.volume = volume;
        this.attempts = attempts;
        this.keyboardMode = keyboardMode;
    } // end constructor
    
    // -------------------------------------------------------------------------

    public Difficulty getDifficulty() {
        return difficulty;
    } // end method

    // -------------------------------------------------------------------------
    
    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    } // end method
    
    // -------------------------------------------------------------------------

    public int getVolume() {
        return volume;
    } // end method
    
    // -------------------------------------------------------------------------

    public void setVolume(int volume) {
        this.volume = volume;
    } // end method
    
    // -------------------------------------------------------------------------

    public int getAttempts() {
        return attempts;
    } // end method
    
    // -------------------------------------------------------------------------

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    } // end method
    
    // -------------------------------------------------------------------------

    public Boolean getKeyboardMode() {
        return keyboardMode;
    } // end method
    
    // -------------------------------------------------------------------------

    public void setKeyboardMode(Boolean keyboardMode) {
        this.keyboardMode = keyboardMode;
    } // end method
} // end class