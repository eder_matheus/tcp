import java.util.Collections;
import java.util.List;

public class Ranking {
    private List<Player> listOfPlayers;
    private List<Integer> listOfScores;

    //--------------------------------------------------------------------------

    public Ranking(List<Player> listOfPlayers, List<Integer> listOfScores) {
        this.listOfPlayers = listOfPlayers;
        this.listOfScores = listOfScores;
    } // end constructor
    
    //--------------------------------------------------------------------------
    
    public List<Player> getListOfPlayers() {
        return listOfPlayers;
    } // end method

    //--------------------------------------------------------------------------
    
    public void setListOfPlayers(List<Player> listOfPlayers) {
        this.listOfPlayers = listOfPlayers;
    } // end method

    //--------------------------------------------------------------------------
    
    public List<Integer> getListOfScores() {
        return listOfScores;
    } // end method

    //--------------------------------------------------------------------------
    
    public void setListOfScores(List<Integer> listOfScores) {
        this.listOfScores = listOfScores;
    } // end method
    
    //--------------------------------------------------------------------------
    
    public void addPlayer(Player newPlayer, int newScore) {
        this.listOfPlayers.add(newPlayer);
        this.listOfScores.add(newScore);
    } // end method
    
    //--------------------------------------------------------------------------
    
    public void sortRanking() {
        Collections.sort(this.listOfPlayers, Collections.reverseOrder());
        // TO DO
    } // end method
    
    //--------------------------------------------------------------------------
    
    public void printRanking() {
        System.out.println("| Position |\t Player |\t Score |");
        for (int i = 0; i < this.listOfPlayers.size(); i++) {
            System.out.println("| " + (i+1) + " |\t " + this.listOfPlayers.get(i).getName() + 
                    " |\t " + this.listOfScores.get(i) + " |");
        } // end for
    } // end method
    
    //--------------------------------------------------------------------------

    public void resetRanking() {
        this.listOfPlayers.clear();
        this.listOfScores.clear();
    } // end method
} // end class